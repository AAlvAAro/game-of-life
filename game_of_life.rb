require 'pry'

class  Cell
  attr_reader :i, :j, :state

  def initialize(position, state, board)
    @i = position[:i]
    @j = position[:j]
    @state = state
    @board = board
  end

  def alive?
    state == 1
  end

  def dead?
    state.zero?
  end

  def update_state
    #Get all game cells to check neighbors
    @cells = @board.cells
    neighbor_count = calculate_neighbor_count

    if dead? && neighbor_count == 3
      @state = 1
    elsif alive? && [2, 3].include?(neighbor_count)
      @state = 1
    else
      @state = 0
    end
  end

  private

  def safe_cell_value(i, j)
    return 0 if i < 0 || j < 0

    row = @cells[i]
    return 0 if row.nil?

    row[j]&.state || 0
  end

  def calculate_neighbor_count
    top_left = safe_cell_value(i - 1, j - 1)
    top_center = safe_cell_value(i - 1, j)
    top_right = safe_cell_value(i - 1, j + 1)
    left = safe_cell_value(i, j - 1)
    right = safe_cell_value(i, j + 1)
    bottom_left = safe_cell_value(i + 1, j - 1)
    bottom_center = safe_cell_value(i + 1, j)
    bottom_right = safe_cell_value(i + 1, j + 1)

    neighbors = [
      top_left,
      top_center,
      top_right,
      left,
      right,
      bottom_left,
      bottom_center,
      bottom_right
    ]

    neighbors.count(1)
  end
end

class Board
  attr_reader :cells

  def initialize(state)
    @initial_state = state
    @size = state.size
    @cells = create_matrix
    set_initial_state
  end

  def update
    cells.flatten.each do |cell|
      cell.update_state
    end
  end

  private

  def create_matrix
    Array.new(@size) { Array.new(@size) }
  end

  def set_initial_state
    @initial_state.each.with_index do |row, i|
      row.each.with_index do |value, j|
        position = { i: i, j: j }
        cells[i][j] = Cell.new(position, value, self)
      end
    end
  end
end

class Game
  attr_reader :board

  def create_board(initial_state)
    @board = Board.new(initial_state)
  end

  def advance
    board.update
  end
end

# initial_state = [[1, 0, 0], [1, 1, 0], [0, 0, 1]]
# initial_state = [[0, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]]
#initial_state = [[1, 1, 1, 1], [1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1]]
initial_state = [[1, 0, 1, 0], [0, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 1]]

game = Game.new
game.create_board(initial_state)

game.advance

10.times do
  game.advance
end

100.times do
  game.advance
end
